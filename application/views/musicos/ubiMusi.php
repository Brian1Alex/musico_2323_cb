<div class="contact_section layout_padding">
    <h1 class="contact_taital" Align="center">Localizaciones de las Sucursales </h1>
    <p class="contact_text" Align="center">Una pequeña referencia de lor remitentes de los envios que se han realizado</p>
    <div class="container">
        <div class="row">
            <div class="col md-12">
                <div id="mapaCan" style="height:500px; width:100%; border:2px solid black;"></div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function initMap() {
            var centro = new google.maps.LatLng(-1.3804393775871402, -78.77341064151292);
            var mapaCan = new google.maps.Map(
                document.getElementById('mapaCan'), {
                    center: centro,
                    zoom: 6.5,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                }
            );

            <?php if ($ubiRock) : ?>
                <?php foreach ($ubiRock as $lugarTemp) : ?>
                    var coordeTemporal = new google.maps.LatLng(<?php echo $lugarTemp->lati_can; ?>, <?php echo $lugarTemp->longi_can; ?>);
                    var marcador = new google.maps.Marker({
                        position: coordeTemporal,
                        title: "<?php echo $lugarTemp->nom_can; ?>",
                        map: mapaCan,
                        icon: "<?php echo base_url(); ?>/assets/images/rock.png"
                    });
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if ($ubiPop) : ?>
                <?php foreach ($ubiPop as $lugarTempo) : ?>
                    var coordeTemporal = new google.maps.LatLng(<?php echo $lugarTempo->lati_can; ?>, <?php echo $lugarTempo->longi_can; ?>);
                    var marcador = new google.maps.Marker({
                        position: coordeTemporal,
                        title: "<?php echo $lugarTempo->nom_can; ?>",
                        map: mapaCan,
                        icon: "<?php echo base_url(); ?>/assets/images/pop.png"
                    });
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if ($ubiReg) : ?>
                <?php foreach ($ubiReg as $lugarTempo) : ?>
                    var coordeTemporal = new google.maps.LatLng(<?php echo $lugarTempo->lati_can; ?>, <?php echo $lugarTempo->longi_can; ?>);
                    var marcador = new google.maps.Marker({
                        position: coordeTemporal,
                        title: "<?php echo $lugarTempo->nom_can; ?>",
                        map: mapaCan,
                        icon: "<?php echo base_url(); ?>/assets/images/reg.png"
                    });
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if ($ubiBal) : ?>
                <?php foreach ($ubiBal as $lugarTempo) : ?>
                    var coordeTemporal = new google.maps.LatLng(<?php echo $lugarTempo->lati_can; ?>, <?php echo $lugarTempo->longi_can; ?>);
                    var marcador = new google.maps.Marker({
                        position: coordeTemporal,
                        title: "<?php echo $lugarTempo->nom_can; ?>",
                        map: mapaCan,
                        icon: "<?php echo base_url(); ?>/assets/images/bal.png"
                    });
                <?php endforeach; ?>
            <?php endif; ?>

        }
    </script>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="btn_main"><a href="<?php echo site_url() ?>/musicos/nueMusi">Agregar Nuevo</a></div>
            </div>
            <div class="col-md-6">
                <div class="btn_main active"><a href="<?php echo site_url() ?>/musicos/listMusi">Listado de Musicos</a></div>
            </div>
        </div>
    </div>
</div>