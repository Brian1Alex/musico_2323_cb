<h1 class="tit" Align="center">Ingresar una Dignidad </h1>
<br>
<div class="container">
    <form class="" action="<?php echo site_url(); ?>/Musicos/guardaMus" method="post">
        <div class="row">
            <div class="col-md-4">
                <label for="">Nombre:</label>
                <br>
                <input type="text" placeholder="Ingrese el nombre del cantante" class="form-control" name="nom_can" value="">
            </div>
            <div class="col-md-4">
                <label for="">Apellido:</label>
                <br>
                <input type="text" placeholder="Ingrese el Apellido del cantante" class="form-control" name="ape_can" value="">
            </div>
            <div class="col-md-4">
                <label for="">Apodo:</label>
                <br>
                <input type="text" placeholder="Ingrese el Apodo con el que es conocido" class="form-control" name="apod_can" value="">
            </div>
            <div class="col-md-4">
                <label for="">Costo de Contrato:</label>
                <br>
                <input type="number" placeholder="Ingrese es costo de contrato" class="form-control" name="contra_can" value="">
            </div>
            <div class="col-md-4">
                <label for="genero_id">Genero: </label>
                <select name="genero_id" class="form-control" id="genero_id" required>
                    <?php foreach($generos as $genero): ?>
                        <option value="<?php echo $genero->id_gen; ?>"><?php echo $genero->nom_gen ?></option>
                    <?php endforeach;?>
                </select>
            </div>

            <div class="col-md-4">
                <label for="">Fecha de Nacimiento:</label>
                <br>
                <input type="date" placeholder="ingresar la fechha de nacimiento" class="form-control" name="naci_can" value="">
            </div>
            

            <div class="col-md-6">
                <label for="">Latitud:</label>
                <br>
                <input type="text" readonly placeholder="Ingrese la Latitud de donde se encuentra su cede" id="lati" class="form-control" name="lati_can" value="">
            </div>
            <div class="col-md-6">
                <label for="">Longitud:</label>
                <br>
                <input type="text" readonly placeholder="Ingrese la Longitud de donde se encuentra su cede" id="longi" class="form-control" name="longi_can" value="">
                <br>
            </div>

            <div class="row">
                <div class="col-md-12" Align="center">
                    <div id="mapaUbi" style="height: 450px; width:100%; border:2px solid black;" Align="center"></div>
                </div>
            </div>

            <script type="text/javascript">
                function initMap() {
                    var cen = new google.maps.LatLng(-1.3804393775871402, -78.77341064151292);
                    var mapa1 = new google.maps.Map(
                        document.getElementById('mapaUbi'), {
                            center: cen,
                            zoom: 7,
                            mapTypeId: google.maps.MapTypeId.HYBRYD
                        }
                    );

                    var marcador = new google.maps.Marker({
                        position: cen,
                        map: mapa1,
                        title: "Seleccione la direccion",
                        icon: "<?php echo base_url(); ?>/assets/images/dragg.png",
                        draggable: true

                    });
                    google.maps.event.addListener(marcador, 'dragend', function() {
                        //alert("Se acabo de arrastrar")
                        document.getElementById('lati').value = this.getPosition().lat();
                        document.getElementById('longi').value = this.getPosition().lng();
                    });
                }
            </script>


        </div>
        <br>
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
                GUARDAR
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/musicos/listMusi" class="btn btn-danger">CANCELAR</a>
        </div>
    </form>
</div>