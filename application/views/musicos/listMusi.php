<h1 class="tit" Align="center">Listado de cantantes </h1>

<?php if ($cantantes) : ?>
    <table class="table table-striped" >
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>APODO</th>
                <th>PRECIO CONTRATO</th>
                <th>FECHA DE NACIMIENTO</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>GENERO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody Align="center">
            <?php foreach ($cantantes as $filaTemporal) : ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_can; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nom_can; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->ape_can; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->apod_can; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->contra_can; echo "<p>$</p>"; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->naci_can; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->lati_can; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->longi_can; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->genero_id; ?>
                    </td>
                    <td class="text-center">
                        <a href="<?php echo site_url(); ?>/musicos/editaMusi/<?php echo $filaTemporal->id_can; ?>" title="Editar Cantante"><i class="glyphicon  glyphicon-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/Musicos/eliminaMusi/<?php echo $filaTemporal->id_can; ?>" title="Eliminar Cantante" style="color:red;">
                            <i class="glyphicon  glyphicon-trash"></i>
                        </a>

                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
<?php else : ?>
    <h1 Align="center">No posee Cantantes ._.</h1>
<?php endif; ?>

<div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="btn_main"><a href="<?php echo site_url() ?>/musicos/nueMusi">Agregar Nuevo</a></div>
            </div>
            <div class="col-md-6">
                <div class="btn_main active"><a href="<?php echo site_url() ?>/musicos/ubiMusi">Ubicaciones</a></div>
            </div>
        </div>
    </div>