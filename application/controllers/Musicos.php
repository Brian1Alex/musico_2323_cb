<?php
class Musicos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Musico');
    }

    public function nueMusi()
    {
        $datos['generos'] = $this->Musico->obteGen();
        $this->load->view('header');
        $this->load->view('musicos/nueMusi', $datos);
        $this->load->view('footer');
    }

    public function listMusi()
    {
        $datos['cantantes'] = $this->Musico->obtener();
        $this->load->view('header');
        $this->load->view('musicos/listMusi', $datos);
        $this->load->view('footer');
    }

    public function editaMusi($id_can)
    {
        $data['generos'] = $this->Musico->obteGen();
        $data["editarCan"] = $this->Musico->obtenerPorId($id_can);
        $this->load->view('header');
        $this->load->view('musicos/editaMusi', $data);
        $this->load->view('footer');
    }

    public function ubiMusi()
    {
        $mapaMusi["ubiRock"] = $this->Musico->ubicaRock();
        $mapaMusi["ubiPop"] = $this->Musico->ubicaPop();
        $mapaMusi["ubiReg"] = $this->Musico->ubicaReg();
        $mapaMusi["ubiBal"] = $this->Musico->ubicaBal();
        $this->load->view('header');
        $this->load->view('musicos/ubiMusi', $mapaMusi);
        $this->load->view('footer');
    }

    public function ubiRock()
    {
        $mapaRock["cantaRock"] = $this->Musico->ubicaRock();
        $this->load->view('header');
        $this->load->view('musicos/ubiRock', $mapaRock);
        $this->load->view('footer');
    }

    public function ubiPop()
    {
        $mapaPop["cantaPop"] = $this->Musico->ubicaPop();
        $this->load->view('header');
        $this->load->view('musicos/ubiPop', $mapaPop);
        $this->load->view('footer');
    }

    public function ubiReg()
    {
        $mapaReg["cantaReg"] = $this->Musico->ubicaReg();
        $this->load->view('header');
        $this->load->view('musicos/ubiReg', $mapaReg);
        $this->load->view('footer');
    }

    public function ubiBal()
    {
        $mapaBal["cantaBal"] = $this->Musico->ubicaBal();
        $this->load->view('header');
        $this->load->view('musicos/ubiBal', $mapaBal);
        $this->load->view('footer');
    }




    public function guardaMus()
    {
        $datosNueMusi = array(
            "nom_can" => $this->input->post('nom_can'),
            "ape_can" => $this->input->post('ape_can'),
            "apod_can" => $this->input->post('apod_can'),
            "contra_can" => $this->input->post('contra_can'),
            "naci_can" => $this->input->post('naci_can'),
            "lati_can" => $this->input->post('lati_can'),
            "longi_can" => $this->input->post('longi_can'),
            "genero_id" => $this->input->post('genero_id')
        );
        if ($this->Musico->insertar($datosNueMusi)) {
            redirect('musicos/listMusi');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    public function eliminaMusi($id_can)
    {
        if ($this->Musico->borrar($id_can)) {
            redirect('musicos/listMusi');
        } else {
            echo "ERROR AL BORRAR :( ";
        }
    }

    public function actualCan()
    {
        $datosEditados = array(
            "nom_can" => $this->input->post('nom_can'),
            "ape_can" => $this->input->post('ape_can'),
            "apod_can" => $this->input->post('apod_can'),
            "contra_can" => $this->input->post('contra_can'),
            "naci_can" => $this->input->post('naci_can'),
            "lati_can" => $this->input->post('lati_can'),
            "longi_can" => $this->input->post('longi_can'),
            "genero_id" => $this->input->post('genero_id')
        );
        $id_can = $this->input->post("id_can");
        if ($this->Musico->actualizar($id_can, $datosEditados)) {
            redirect("musicos/listMusi");
        } else {
            echo "ERROR AL EDITAR :( ";
        }
    }
}
