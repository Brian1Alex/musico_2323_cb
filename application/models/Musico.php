<?php
class Musico extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("cantante", $datos);
    }

    function obtener()
    {
        $listCantan = $this->db->get("cantante");
        if ($listCantan->num_rows() > 0) {
            return $listCantan->result();
        } else {
            return false;
        }
    }

    function borrar($id_can)
    {
        $this->db->where("id_can", $id_can);
        return $this->db->delete("cantante");
    }

    function actualizar($id_can, $dat)
    {
        $this->db->where("id_can", $id_can);
        return $this->db->update('cantante', $dat);
    }


    function ubicaRock()
    {
        $listaUbiSie=$this->db->select()->where('genero_id','1')->get("cantante");
        if ($listaUbiSie->num_rows() > 0) {
            return $listaUbiSie->result();
        }
        return false;
    }

    function ubicaPop()
    {
        $listadoUbiCos=$this->db->select()->where('genero_id','2')->get("cantante");
        if ($listadoUbiCos->num_rows() > 0) {
            return $listadoUbiCos->result();
        }
        return false;
    }

    function ubicaReg()
    {
        $listadoUbiCos=$this->db->select()->where('genero_id','3')->get("cantante");
        if ($listadoUbiCos->num_rows() > 0) {
            return $listadoUbiCos->result();
        }
        return false;
    }

    function ubicaBal()
    {
        $listadoUbiCos=$this->db->select()->where('genero_id','4')->get("cantante");
        if ($listadoUbiCos->num_rows() > 0) {
            return $listadoUbiCos->result();
        }
        return false;
    }

    function obteGen()
    {
        $listaGen = $this->db->get("genero");
        if ($listaGen->num_rows() > 0) {
            return $listaGen->result();
        } else {
            return false;
        }
    }

    function ObtenerPorId($id_can)
    {
        $this->db->where("id_can", $id_can);
        $cantante = $this->db->get("cantante");
        if ($cantante->num_rows() > 0) {
            return $cantante->row();
        }
        return false;
    }


}